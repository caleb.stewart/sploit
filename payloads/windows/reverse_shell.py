from pwn import *
from payloads import BasePayload

class Payload(BasePayload):
    def __init__(self, config):
        super(Payload, self).__init__(config)

    def generate(self, config):
        return ''
