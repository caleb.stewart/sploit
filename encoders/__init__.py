from pwn import *

class BaseEncoder(object):

    options = []

    def __init__(self, config):
        pass

    def encode(self, data):
        log.error('encoder: no encoding function specified')

    def decode(self, data):
        log.error('encoder: no decoding function specified')

    @classmethod
    def add_arguments(cls, parser):
        pass