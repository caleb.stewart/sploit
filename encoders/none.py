from encoders import BaseEncoder

class Encoder(BaseEncoder):

    def __init__(self, config):
        super(Encoder, self).__init__(config)

    def encode(self, data):
        return data

    def decode(self, data):
        return data
