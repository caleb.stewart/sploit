from pwn import *

# Wraps a tube object for some sort of C2 with a target
# Common example would be a UnixShellConnection. Provides an
# abstracted interface to send basic commands (e.g. ls,
# cat, rm, as well as executing other commands)
class Connection(object):

    def __init__(self, tube):
        super(Connection, self).__init__()
        self.tube = tube

    def is_open(self):
        return self.tube.connected()

    def close(self):
        self.tube.close('send')
        self.tube.close('recv')

    def listdir(self, path=None):
        pass

    def chdir(self, path):
        pass

    def cwd(self):
        pass

    def cat(self, path):
        pass

    def rm(self, path):
        pass

    def execute(self, command):
        pass

    def send_raw(self, script):
        self.tube.send(script)

    def interactive(self, prompt=''):
        return self.tube.interactive(prompt=prompt)

class UnixShellConnection(Connection):

    def __init__(self, tube):
        super(UnixShellConnection, self).__init__(tube)

    def listdir(self, path=None):
        self.tube.recv(1024)
        self.tube.sendline('ls \'{0}\'; echo \'__!!END!!__\''.format(path))
        # The last line is the indicator text we echoed. We remove it first, then split the files
        files = self.tube.recvuntil('__!!END!!__').split('\n__!!END!!__')[0].split()
        return files

    def chdir(self, path):
        self.tube.recv(1024)
        self.tube.sendline('cd {0}'.format(path))
        self.tube.recv()

    def cwd(self):
        self.tube.recv(1024)
        self.tube.sendline('pwd; echo \'__!!END!!__\'')
        return self.tube.recvuntil('__!!END!!__').split('__!!END!!__')[0]

    def cat(self, path):
        self.tube.recv(1024)
        self.tube.sendline("cat '{0}'; echo '__!!END!!__'".format(path))
        return self.tube.recvuntil('__!!END!!__').split('__!!END!!__')[0]

    def rm(self, path):
        cmd = "rm -rf '{0}'".format(path)
        self.tube.sendline(cmd)
        return

    def send_raw(self, script):
        self.tube.send(script+'\n')

    def execute(self, command):
        self.tube.recv(1024)
        self.tube.sendline("{0}; echo '__!!END!!__'".format(command))
        return self.tube.recvuntil('__!!END!!__').split('__!!END!!__')[0]
        